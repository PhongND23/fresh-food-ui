import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { HeaderComponent } from './header/header.component';
import { ShopComponent } from './shop/shop.component';
import { HomeComponent } from './home/home.component';
import { FooterComponent } from './footer/footer.component';
import { CartComponent } from './cart/cart.component';
import { BlogComponent } from './blog/blog.component';
import { BlogDetailComponent } from './blog-detail/blog-detail.component';
import { ContactComponent } from './contact/contact.component';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { LoginComponent } from './login/login.component';
import { FormsModule } from '@angular/forms';
import { RegisterComponent } from './register/register.component';
import { InfoComponent } from './info/info.component';
import { PopupLoginComponent } from './popup-login/popup-login.component';
import { OrderFollowComponent } from './order-follow/order-follow.component';
import { ShopDetailComponent } from './shop-detail/shop-detail.component';
import { ProfileComponent } from './profile/profile.component';
import { PostComponent } from './post/post.component';
@NgModule({
  declarations: [
    AppComponent,
    ProductDetailComponent,
    HeaderComponent,
    ShopComponent,
    HomeComponent,
    FooterComponent,
    CartComponent,
    BlogComponent,
    BlogDetailComponent,
    ContactComponent,
    LoginComponent,
    RegisterComponent,
    InfoComponent,
    PopupLoginComponent,
    OrderFollowComponent,
    ShopDetailComponent,
    ProfileComponent,
    PostComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
