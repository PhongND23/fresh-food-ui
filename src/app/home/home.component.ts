import { Component, OnInit } from '@angular/core';
import { ProductCategoryService } from '../service/productcategoryservice';
import { ProductService } from '../service/productservice';
import { CartService } from '../service/cartservice';
import { Router } from '@angular/router';
import { Message } from '../message/message';
import { BlogService } from '../service/blogservice';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  category: any = [];
  categoryId: any = '';
  products: any = [];
  account:any={}
  name:any=''
  blogs:any=[]
  constructor(
    private categoryService: ProductCategoryService,
    private productService: ProductService,
    private cartService: CartService,
    private message:Message,
    private router:Router,
    private blogService:BlogService
  ) {}

  ngOnInit(): void {
    this.categoryService.drop().subscribe((res: any) => (this.category = res));
    this.categoryId = '';
    this.getProducts();
    var json=localStorage.getItem('account')
    this.account=json!==null?JSON.parse(json):null
    this.getBlog()

  }

  getProducts() {
    this.productService
      .findAll('','', this.categoryId, '','','', 0, 8,'createdDate','desc')
      .subscribe((res: any) => {
        this.products = res.content;
      });
  }
  setCategoryId(id: any) {
    this.categoryId = id;
    this.getProducts();
  }

  getAll() {
    this.categoryId = '';
    this.getProducts();
  }
  addToCart(id:any)
  {
    this.cartService.addToCart(this.account.id,id).subscribe((res:any)=>{
      this.message.addToCart()
    },(e:any)=>{
      this.message.fail(e.error.message)
    })
  }

  changePage(id:any)
  {
    this.router.navigate(['shop',id])
  }

  changePageByNameSearch()
  {
    this.router.navigate(['shop',this.name])
  }

  getBlog()
  {
    this.blogService.findAll('','','',true,0,3).subscribe((res:any)=>{
      this.blogs=res.content;
    })
  }
}
