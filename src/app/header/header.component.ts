import { Component, OnInit } from '@angular/core';
import { Message } from '../message/message';
import { CartService } from '../service/cartservice';
import { AccountService } from '../service/accountservice';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  constructor(
    private message: Message,
    private service: AccountService,
    private cartService: CartService,
    private accountService: AccountService
  ) {}

  account: any = {};
  login: any = {};
  isShowLogin: boolean = true;
  isShowRegis: boolean = false;
  count: any = 0;
  products: any = [];
  total: any = 0;

  ngOnInit(): void {
    this.account = this.accountService.getCurrentUser();
    if (this.account) {
      this.cartService.getProducts(this.account).subscribe((data: any) => {
        this.count = data.length;
        this.products = data;
        for (var item of this.products) {
          this.total += item.promotionPrice * item.quantityCart;
        }
      });
    }
  }
  show() {
    this.isShowLogin = true;
  }
  authen() {
    this.service.login(this.login).subscribe(
      (res: any) => {
        localStorage.setItem('account', JSON.stringify(res));
        this.message.success('Đăng nhập thành công');
        setTimeout(() => {
          window.location.reload();
        }, 700);
      },
      (error: any) => {
        this.message.error(error.error.message);
      }
    );
  }
  logout() {
    localStorage.removeItem('account');
    window.location.reload();
  }

  showRegis() {
    this.isShowRegis = true;
    this.isShowLogin = false;
  }


}
