import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import Swal from 'sweetalert2';
@Injectable({
  providedIn: 'root',
})
export class Message {
  addToCart() {
    Swal.fire({
      showConfirmButton: false,
      html: `<i class="fa fa-check-circle-o" style="color:white;font-size:100px;"></i> <p style="color:white;font-size:17px;">Đã thêm vào giỏ hàng</p>`,
      background: 'rgba(0,0,0,0.6)',
      color: 'white',
      timer: 700,
      backdrop: `rgba(225,225,225,0)`,
      position: 'center',
    });
  }

  paymentSuccess() {
    Swal.fire({
      showConfirmButton: false,
      html: `<i class="fa fa-check-circle" style="color:green;"></i> Đặt đơn hàng thành công`,
      background: 'rgba(0,0,0,0.5)',
      color: 'white',
      timer: 700,
      backdrop: `rgba(225,225,225,0)`,
    });
  }

  order(list: any) {
    Swal.fire({
      html: ``,
    });
  }
  success(text: any) {
    Swal.fire({
      showConfirmButton: false,
      html: `<i class="fa fa-check-circle-o" style="color:white;font-size:100px;"></i> <p style="color:white;font-size:17px;">${text}</p>`,
      background: 'rgba(0,0,0,0.6)',
      color: 'white',
      timer: 700,
      backdrop: `rgba(225,225,225,0)`,
      position: 'center',
    });
  }

  error(text: any) {
    Swal.fire({
      showConfirmButton: false,
      html: `<i class="fa fa-exclamation-triangle"></i> ${text}`,
      background: '#ff4d4d',
      color: 'white',
      timer: 1000,
      customClass: 'swal-wide',
      position: 'top-right',
      backdrop: `rgba(225,225,225,0.0)`,
    });
  }

  warn(text: any) {
    Swal.fire({
      showConfirmButton: false,
      html: `<i class="fa fa-frown-o" style="color:white;font-size:100px;"></i> <p style="color:white;font-size:17px;">${text}</p>`,
      background: 'rgba(0,0,0,0.6)',
      color: 'white',
      timer: 1000,
      backdrop: `rgba(225,225,225,0)`,
      position: 'center',
    });
  }
  fail(text:any) {
    Swal.fire({
      showConfirmButton: false,
      html: `<i class="fa fa-frown-o" style="color:white;font-size:100px;"></i> <p style="color:white;font-size:17px;">${text}</p>`,
      background: 'rgba(0,0,0,0.6)',
      color: 'white',
      timer: 700,
      backdrop: `rgba(225,225,225,0)`,
      position: 'center',
    });
  }
}
