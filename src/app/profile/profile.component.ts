import { Component, OnInit } from '@angular/core';
import { AccountService } from '../service/accountservice';
import { Message } from '../message/message';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
})
export class ProfileComponent implements OnInit {
  constructor(
    private accountService: AccountService,
    private message: Message
  ) {}

  account: any = {};
  info: boolean = true;
  userDTO: any = {};
  submited: any = false;
  avatar: any;
  ngOnInit(): void {
    this.account = this.accountService.getCurrentUser();
  }

  hashPassword(password: string) {
    return '*'.repeat(password.length);
  }
  logout() {
    localStorage.removeItem('account');
    window.location.reload();
  }
  edit() {
    this.info = false;
    this.avatar = this.account.avatar;
  }

  backToProfile()
  {
    this.info=true;
    this.avatar=this.account.avatar
  }

  save() {
    this.submited = true;
    if (
      this.account.userDTO.fullName &&
      this.account.userDTO.email &&
      this.account.userDTO.address &&
      this.account.userDTO.phoneNumber &&
      this.account.userName &&
      this.account.password
    ) {
      if (this.avatar) this.account.avatar = this.avatar;
      this.account.status = true;
      this.account.roleId = 2;
      this.account.userDTO.gender = this.userDTO.gender;
      this.accountService.edit(this.account.id, this.account).subscribe(
        (res: any) => {
          localStorage.setItem('account', JSON.stringify(this.account));
          this.message.success('Cập nhật thành công');
          setTimeout(() => {
            window.location.reload();
          }, 800);
        },
        (error: any) => {
          this.message.error(error.error.message);
        }
      );
    }
  }
  myUploader(event: any) {
    for (let file of event.target.files) {
      this.handleFileSelect(file);
    }
  }

  handleFileSelect(file: any) {
    if (file) {
      var reader = new FileReader();
      reader.onload = this._handleReaderLoaded.bind(this);
      reader.readAsBinaryString(file);
    }
  }

  _handleReaderLoaded(readerEvt: any) {
    var binaryString = readerEvt.target.result;
    this.avatar = 'data:image/jpg;base64,' + btoa(binaryString);
  }

  setGender(value: any) {
    this.userDTO.gender = value;
    console.log(this.userDTO.gender);
  }
}
