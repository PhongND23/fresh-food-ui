import { Component, OnInit } from '@angular/core';
import { CartService } from '../service/cartservice';
import { AccountService } from '../service/accountservice';
import { Message } from '../message/message';
@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css'],
})
export class CartComponent implements OnInit {
  constructor(
    private cartService: CartService,
    private accountService: AccountService,
    private message: Message
  ) {}

  account: any = {};
  products: any = [];
  selectedProducts: any = [];
  selectAll: boolean = false;
  otherAddress: boolean = false;
  order: any = {};
  ngOnInit(): void {
    this.account = this.accountService.getCurrentUser();
    if(this.account)
    {
      this.cartService.getProducts(this.account).subscribe((data: any) => {
        this.products = data;
      });
    }
  }
  minius(item: any) {
    if (item.quantityCart - 1 === 0) {
      this.remove(item);
    } else {
      item.quantityCart -= 1;
      this.cartService.updateCart(this.products).subscribe();
    }
  }
  plus(item: any) {
    if (item.quantityCart < item.quantity) {
      item.quantityCart += 1;
      this.cartService.updateCart(this.products).subscribe();
    }
  }

  remove(item: any) {
    let index = this.products.findIndex((e: any) => e.id === item.id);
    if (index > -1) {
      var ids: any = [];
      ids.push(item.cartId);
      this.cartService.removeToCart(ids).subscribe((res: any) => {
        this.products.splice(index, 1);
      });
    }
  }

  getTotal() {
    var total = 0;
    for (var value of this.selectedProducts) {
      total += value.promotionPrice * value.quantityCart;
    }
    return total;
  }

  selectCart(item: any) {
    let index = this.selectedProducts.findIndex((e: any) => e.id === item.id);
    if (index > -1) {
      this.selectedProducts.splice(index, 1);
    } else {
      this.selectedProducts.push(item);
    }
    console.log(this.selectedProducts);
  }
  getAll() {
    this.selectAll = !this.selectAll;
    if (this.selectAll) {
      this.selectedProducts = [];
      this.selectedProducts.push(...this.products);
    } else {
      this.selectedProducts = [];
    }
    console.log(this.selectedProducts);
  }
  changeQuantity(item: any) {
    if (item.quantityCart <= 0) this.message.error('Số lượng không hợp lệ');
    else {
      this.cartService.updateCart(this.products).subscribe();
    }
  }

  changeAddress() {
    this.otherAddress = !this.otherAddress;
  }

  payment()
  {
    if(!this.otherAddress)
    {
      this.order.phone=this.account.userDTO.phoneNumber
      this.order.address=this.account.userDTO.address
      this.order.fullName=this.account.userDTO.fullName
    }
    this.order.customerId=this.account.id
    this.order.products=this.selectedProducts;
    this.cartService.payment(this.order).subscribe((res:any)=>{
      this.message.paymentSuccess()
      setTimeout(() => {
        window.location.reload()
      }, 700);
    })

  }
}
