import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BlogService } from '../service/blogservice';
import { Router } from '@angular/router';
@Component({
  selector: 'app-blog-detail',
  templateUrl: './blog-detail.component.html',
  styleUrls: ['./blog-detail.component.css'],
})
export class BlogDetailComponent implements OnInit {
  constructor(
    private router: ActivatedRoute,
    private blogService: BlogService,
    private route:Router
  ) {}

  id: any;
  blog: any = {};
    categories:any=[]
    newestBlog:any=[]
    sameBlogs:any=[]
  ngOnInit(): void {
    this.id = this.router.snapshot.paramMap.get('id');
    this.getDetail(this.id);
    this.getNewestBlog();
    this.blogService.drop().subscribe((res:any)=>{
      this.categories=res
    })
  }

  getDetail(id: any) {
    this.blogService.findAll('', '', id,'', 0, 1).subscribe((res: any) => {
      this.blog = res.content[0];
    this.getSameBlogs()

    });
  }

  getNewestBlog()
  {
    this.blogService.findAll('', '', '',true  , 0, 4).subscribe((res: any) => {
      console.log(this.id)
      this.newestBlog = res.content;
      this.newestBlog=this.newestBlog.filter((item:any)=>item.id-this.id!==0);
    });
  }

  getSameBlogs()
  {
    this.blogService.findAll('', this.blog.categoryId, '',false, 0, 3).subscribe((res: any) => {
      this.sameBlogs = res.content;
      this.sameBlogs=this.sameBlogs.filter((item:any)=>item.id-this.id!==0);
    });
  }

  changePage(id:any)
  {
    this.id=id
    this.getDetail(this.id);
    this.getNewestBlog();
    this.blogService.drop().subscribe((res:any)=>{
      this.categories=res
      window.scrollTo(0,0)

    })
  }

}
