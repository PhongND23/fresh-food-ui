import { Component, OnInit } from '@angular/core';
import { ProductService } from '../service/productservice';
import { CartService } from '../service/cartservice';
import { AccountService } from '../service/accountservice';
import { ProductCategoryService } from '../service/productcategoryservice';
import { Message } from '../message/message';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import {} from '../service/cartservice';
@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.css'],
})
export class ShopComponent implements OnInit {
  constructor(
    private productService: ProductService,
    private cartService: CartService,
    private accountService: AccountService,
    private message: Message,
    private category: ProductCategoryService,
    private router:Router,
    private routerActive:ActivatedRoute
  ) {}

  products: any[] = [];
  totalPages: number = 0;
  page: number = 0;
  account: any = {};
  categories: any[] = [];
  categoryId = '';
  fromPrice = '';
  toPrice = '';
  name=''
  sort: any = 'name';
  vector: any = 'asc';
  totalElements: any = 0;

  ngOnInit(): void {
    var categoryId=null;
    categoryId = this.routerActive.snapshot.paramMap.get('id');
    var tryParse=Number(categoryId);
    if(categoryId&&tryParse) this.categoryId=categoryId;
    else
    {
      var name=null
      name=this.routerActive.snapshot.paramMap.get('id');
      if(name) this.name=name;
    }



    this.findAll();
    this.account = this.accountService.getCurrentUser();
    this.category.drop().subscribe((res: any) => {
      this.categories = res;
    });
  }
  counter(i: number) {
    return new Array(i);
  }

  findAll() {
    this.productService
      .findAll(
        this.name,
        '',
        this.categoryId,
        '',
        this.fromPrice,
        this.toPrice,
        this.page,
        9,
        this.sort,
        this.vector
      )
      .subscribe((res: any) => {
        this.products = res.content;
        this.totalPages = res.totalPages;
        this.totalElements = res.totalElements;
      });
  }
  getPage(page: any) {
    this.page = page;
    this.findAll();
  }
  addToCart(id: any) {
    this.cartService.addToCart(this.account.id, id).subscribe((res: any) => {
      this.message.addToCart();
    },(e:any)=>{
      this.message.fail(e.error.message)
    });
  }
  getCategory(categoryId: any) {
    this.categoryId = categoryId;
    this.findAll();
  }
  getAllCategory() {
    this.categoryId = '';
    this.findAll();
  }

  getByPrice() {
    var fromPrice = this.fromPrice ? this.fromPrice : '';
    var toPrice = this.toPrice ? this.toPrice : '';
    this.fromPrice = fromPrice;
    this.toPrice = toPrice;
    this.findAll();
  }

  getBySort() {
    console.log(this.sort);
    this.findAll();
  }

  clearFilter() {
    this.categoryId = '';
    this.toPrice = '';
    this.fromPrice = '';
    this.sort = 'name';
    this.name=''

    this.findAll();
  }
  getDetail(id:any)
  {
    this.router.navigate(['/shop-detail', id]);
  }

  search()
  {
    this.findAll()
  }
}
