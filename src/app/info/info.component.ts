import { Component, OnInit } from '@angular/core';
import { AccountService } from '../service/accountservice';
import { Message } from '../message/message';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.css']
})
export class InfoComponent implements OnInit {

  constructor(private service:AccountService,private message:Message,private router:Router) { }
  userDTO:any={}
  submited:boolean=false
  account:any={}
  ngOnInit(): void {
  }

  setGender(value:any)
  {
    this.userDTO.gender=value
    console.log(this.userDTO.gender)
  }
  update()
  {
    this.submited=true
    if(this.userDTO.fullName
      &&this.userDTO.email
      &&this.userDTO.address
      &&this.userDTO.phoneNumber
      &&this.userDTO.dateOfBirth
      &&this.account.userName
      &&this.account.password)
      {
        this.account.userDTO=this.userDTO;
        this.account.roleId=2
        this.account.status=true
        this.service.save(this.account).subscribe((res:any)=>{
          localStorage.setItem('account',JSON.stringify(this.account))
        this.message.success('Đăng kí thành công')
        setTimeout(()=>{this.router.navigate([''])},800)
        },(error:any)=>{
          this.message.error(error.error.message)
        })
      }

  }

}
