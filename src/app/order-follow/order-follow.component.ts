import { Component, OnInit } from '@angular/core';
import { OrderService } from '../service/orderservice';
import { AccountService } from '../service/accountservice';
import { Message } from '../message/message';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-order-follow',
  templateUrl: './order-follow.component.html',
  styleUrls: ['./order-follow.component.css'],
})
export class OrderFollowComponent implements OnInit {
  constructor(
    private orderService: OrderService,
    private accountService: AccountService,
    private message: Message
  ) {}

  searchDTO: any = {};
  account: any = {};
  orders: any[] = [];
  order: any = {};
  countDTO: any = {};
  ngOnInit(): void {
    this.account = this.accountService.getCurrentUser();
    this.searchDTO.customerId = this.account.id;
    if (this.searchDTO.customerId) {
      this.searchDTO.orderStatusId=1;
      this.getOrders();
      this.countByStatus()
    }
  }

  bind(item: any) {
    this.order = item;
  }
  setStatus(statusId: any) {
    this.searchDTO.orderStatusId = statusId;
    this.getOrders();
  }

  getOrders() {
    this.orderService.findAll(this.searchDTO).subscribe((res: any) => {
      this.orders = res;
      for (var item of this.orders) {
        var total = 0;
        for (var e of item.products) {
          total += e.currentPrice * e.quantity;
          total += 15000;
        }
        item.total = total;
      }
    });
  }

  countByStatus() {
    this.searchDTO.orderStatusId=''
    this.orderService.findAll(this.searchDTO).subscribe((res: any) => {
      this.getCount(res);
    });
  }

  getCount(list: any) {
    var count1 = 0;
    var count2 = 0;
    var count3 = 0;
    var count4 = 0;
    var count5 = 0;
    for (var item of list) {
      if (item.orderStatusId === 1) count1++;
      if (item.orderStatusId === 2) count2++;
      if (item.orderStatusId === 3) count3++;
      if (item.orderStatusId === 4) count4++;
      if (item.orderStatusId === 5) count5++;
    }
    console.log(list);

    this.countDTO.count1 = count1;
    this.countDTO.count2 = count2;
    this.countDTO.count3 = count3;
    this.countDTO.count4 = count4;
    this.countDTO.count5 = count5;
  }

  cancle(item: any) {
    Swal.fire({
      html: `Bạn có muốn hủy đơn hàng ${item.id} ?`,
      color: 'gray',
      showConfirmButton: true,
      showCancelButton: true,
      confirmButtonText: 'Đồng ý',
      confirmButtonColor: 'orange',
      cancelButtonText: 'Đóng',
    }).then((result) => {
      if (result.isConfirmed) {
        this.orderService.changeStatus(item.id, 5).subscribe((res: any) => {
          this.orderService.cancel(item.id).subscribe((res: any) => {
            this.message.success('Đã hủy đơn hàng');
            setTimeout(() => {
              window.location.reload();
            }, 700);
          });
        });
      } else {
      }
    });
  }
}
