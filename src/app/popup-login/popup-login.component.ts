import { Component, OnInit } from '@angular/core';
import { Message } from '../message/message';
import { AccountService } from '../service/accountservice';

@Component({
  selector: 'app-popup-login',
  templateUrl: './popup-login.component.html',
  styleUrls: ['./popup-login.component.css'],
})
export class PopupLoginComponent implements OnInit {
  constructor(private message: Message, private service: AccountService) {}

  account: any = {};
  login: any = {};
  isShowLogin: boolean = true;
  isShowRegis: boolean = false;
  submitted:boolean=false;
  notValid:boolean=false;
  ngOnInit(): void {
    var json = localStorage.getItem('account');
    this.account = json === null ? null : JSON.parse(json);
  }
  authen() {
    this.submitted=true
    if(this.submitted&&this.login.userName&&this.login.password&&this.login.userName.length<20)
    {
      this.service.login(this.login).subscribe(
        (res: any) => {
          localStorage.setItem('account', JSON.stringify(res));
          this.message.success('Đăng nhập thành công');
          setTimeout(() => {
            window.location.reload();
          }, 700);
        },
        (error: any) => {
          this.notValid=true
        }
      );
    }
  }
  logout() {
    localStorage.removeItem('account');
    window.location.reload();
  }

  showRegis() {
    this.isShowRegis = true;
    this.isShowLogin = false;
  }
  showLogin() {
    this.isShowRegis = false;
    this.isShowLogin = true;
  }
}
