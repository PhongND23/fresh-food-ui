import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root',
})
export class CartService {
  constructor(private http: HttpClient) {}

  addToCart(accountId: any, productId: any) {
    return this.http.get(
      `${environment.url}/cart/add-to-cart/${accountId}/${productId}`
    );
  }
  addToCartWithQuantity(accountId: any, productId: any,quantity:any) {
    return this.http.get(
      `${environment.url}/cart/add-to-cart/${accountId}/${productId}/${quantity}`
    );
  }
  getProducts(account: any): any {
    if(account)
    {
      return this.http.get(`${environment.url}/cart/get-cart/${account.id}`);
    }
    else return []
  }

  removeToCart(ids: any) {
    return this.http.post(`${environment.url}/cart/remove-to-cart`, ids);
  }

  updateCart(list: any) {
    return this.http.post(`${environment.url}/cart/update-cart`, list);
  }

  payment(dto: any) {
    return this.http.post(`${environment.url}/order/payment`, dto);
  }

  buy(dto: any) {
    return this.http.post(`${environment.url}/order/buy`, dto);
  }
}
