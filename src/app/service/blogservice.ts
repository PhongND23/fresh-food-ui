import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root',
})
export class BlogService {
  constructor(private http: HttpClient) {}

  findAll(title: any, categoryId: any,id:any,isHot:any, page: any, size: any): any {
    return this.http.get(
      `${environment.url}/post?id=${id}&categoryId=${categoryId}&title=${title}&isHot=${isHot}&status=${true}&page=${page}&size=${size}&sort=createdDate,desc`
    );
  }

  drop() {
    return this.http.get(`${environment.url}/post-category/drop`);
  }

  edit(id: any, dto: any): any {
    return this.http.put(`${environment.url}/product/${id}`, dto);
  }

  delete(id: any): any {
    return this.http.delete(`${environment.url}/product/${id}`);
  }

}
