import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root',
})
export class CommentService {
  constructor(private http: HttpClient) {}

  findAll(productId: any, postId: any, page: any, size: any): any {
    return this.http.get(
      `${environment.url}/comment?productId=${productId}&postId=${postId}&status=${true}&page=${page}&size=${size}&sort=postedDate,desc`
    );
  }

  allow(accountId: any, productId: any): any {
    return this.http.get(
      `${environment.url}/comment/allow?productId=${productId}&accountId=${accountId}`
    );
  }

  save(comment: any) {
    return this.http.post(`${environment.url}/comment`, comment);
  }
}
