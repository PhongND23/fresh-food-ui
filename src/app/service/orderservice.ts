import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root',
})
export class OrderService {
  constructor(private http: HttpClient) {}

  findAll(dto: any): any {
    return this.http.post(`${environment.url}/order/customer-order`, dto);
  }

  save(dto: any) {
    return this.http.post(`${environment.url}/product`, dto);
  }

  edit(id: any, dto: any): any {
    return this.http.put(`${environment.url}/product/${id}`, dto);
  }

  delete(id: any): any {
    return this.http.delete(`${environment.url}/product/${id}`);
  }

  changeStatus(id: any,statusId:any): any {
    return this.http.get(`${environment.url}/order/change/${id}/${statusId}`);
  }

  cancel(id: any): any {
    return this.http.get(`${environment.url}/order/cancel/${id}`);
  }
}
