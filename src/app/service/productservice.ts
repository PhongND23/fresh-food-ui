import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root',
})
export class ProductService {
  constructor(private http: HttpClient) {}

  findAll(
    name: any,
    id:any,
    categoryId: any,
    accountId: any,
    fromPrice:any,
    tomPrice:any,
    page: any,
    size: any,
    sort:any,
    vector:any
  ): any {
    return this.http.get(
      `${environment.url}/product?id=${id}&name=${name}&categoryId=${categoryId}&accountId=${accountId}&fromPrice=${fromPrice}&toPrice=${tomPrice}&status=${true}&page=${page}&size=${size}&sort=${sort},${vector}`
    );
  }

  save(dto: any) {
    return this.http.post(`${environment.url}/product`, dto);
  }

  edit(id: any, dto: any): any {
    return this.http.put(`${environment.url}/product/${id}`, dto);
  }

  delete(id: any): any {
    return this.http.delete(`${environment.url}/product/${id}`);
  }
  getProductsSmall() {}
}
