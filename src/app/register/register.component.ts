import { Component, OnInit } from '@angular/core';
import { AccountService } from '../service/accountservice';
import { Message } from '../message/message';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent implements OnInit {
  constructor(
    private service: AccountService,
    private message: Message,
    private router: Router
  ) {}
  userDTO: any = {};
  submited: boolean = false;
  account: any = {};
  avatar: any;
  ngOnInit(): void {
    this.userDTO.gender = true;
  }

  setGender(value: any) {
    this.userDTO.gender = value;
    console.log(this.userDTO.gender);
  }
  save() {
    this.submited = true;
    console.log(this.account);
    if (
      this.userDTO.fullName &&
      this.userDTO.email &&
      this.userDTO.address &&
      this.userDTO.phoneNumber &&
      this.account.userName.length>0 &&
      this.account.userName.length<=20 &&
      this.account.password&&
      this.isNumber(this.userDTO.phoneNumber)
    ) {
      this.account.userDTO = this.userDTO;
      this.account.roleId = 2;
      this.account.status = true;
      if (this.avatar) this.account.avatar = this.avatar;

      this.service.save(this.account).subscribe(
        (res: any) => {
         this.getInfo()
        },
        (error: any) => {
          this.message.error(error.error.message);
        }
      );



    }
  }
  getInfo()
  {
    this.service.getAccountByUserName(this.account.userName).subscribe((res:any)=>{
      this.account=res.content[0]
      localStorage.setItem('account', JSON.stringify(this.account));
        this.message.success('Đăng kí thành công');
        setTimeout(() => {
          window.location.reload();
        }, 800);
    })
  }

  myUploader(event: any) {
    for (let file of event.target.files) {
      this.handleFileSelect(file);
    }
  }

  handleFileSelect(file: any) {
    if (file) {
      var reader = new FileReader();
      reader.onload = this._handleReaderLoaded.bind(this);
      reader.readAsBinaryString(file);
    }
  }

  _handleReaderLoaded(readerEvt: any) {
    var binaryString = readerEvt.target.result;
    this.avatar = 'data:image/jpg;base64,' + btoa(binaryString);
  }
  isNumber(value:any)
  {
    if(String(value).includes('.')) return false;
    var ok=Number(value);
    if(ok)
    {
      if(ok<0) return false;
    }
    else return false;
    return true;
  }
}
