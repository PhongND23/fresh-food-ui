import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { BlogDetailComponent } from './blog-detail/blog-detail.component';
import { BlogComponent } from './blog/blog.component';
import { CartComponent } from './cart/cart.component';
import { ContactComponent } from './contact/contact.component';
import { HomeComponent } from './home/home.component';
import { InfoComponent } from './info/info.component';
import { LoginComponent } from './login/login.component';
import { OrderFollowComponent } from './order-follow/order-follow.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { RegisterComponent } from './register/register.component';
import { ShopComponent } from './shop/shop.component';
import { ShopDetailComponent } from './shop-detail/shop-detail.component';

const routes: Routes = [
  {path:'detail',component:ProductDetailComponent},
  {path:'shop/:id',component:ShopComponent},
  {path:'home',component:HomeComponent},
  {path:'cart',component:CartComponent},
  {path:'blog',component:BlogComponent},
  {path:'blog-detail/:id',component:BlogDetailComponent},
  {path:'contact',component:ContactComponent},
  {path:'login',component:LoginComponent},
  {path:'register',component:RegisterComponent},
  {path:'info',component:InfoComponent},
  {path:'order-follow',component:OrderFollowComponent},
  {path:'shop-detail/:id',component:ShopDetailComponent},
  {path:'',component:HomeComponent},



];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
