import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductService } from '../service/productservice';
import { CartService } from '../service/cartservice';
import { AccountService } from '../service/accountservice';
import { CommentService } from '../service/commentservice';
import { Message } from '../message/message';
import { Router } from '@angular/router';
import {} from '../service/cartservice';
import { tick } from '@angular/core/testing';
@Component({
  selector: 'app-shop-detail',
  templateUrl: './shop-detail.component.html',
  styleUrls: ['./shop-detail.component.css'],
})
export class ShopDetailComponent implements OnInit {
  constructor(
    private router: ActivatedRoute,
    private productService: ProductService,
    private cartService: CartService,
    private accountService: AccountService,
    private commentService: CommentService,
    private mesage: Message,
    private rou: Router
  ) {}

  id: any;
  product: any = {};
  account: any = {};
  allow: boolean = false;
  size: any = 5;
  comments: any = [];
  comment: any = {};
  sameProducts: any = [];
  otherAddress: boolean = false;
  order: any = {};
  ngOnInit(): void {
    this.id = this.router.snapshot.paramMap.get('id');
    this.getDetail(this.id);
    this.account = this.accountService.getCurrentUser();
    if (this.account) {
      this.commentService
        .allow(this.account.id, this.id)
        .subscribe((res: any) => {
          this.allow = res;
        });
      this.commentService
        .findAll(this.id, '', 0, this.size)
        .subscribe((res: any) => {
          this.comments = res.content;
        });
    }
  }
  getDetail(id: any) {
    this.productService
      .findAll('', id, '', '', '', '', '', '', '', 'asc')
      .subscribe((res: any) => {
        this.product = res.content[0];
        this.product.quantityCart = 1;
        this.getSameProducts(this.product);
      });
  }

  minius(product: any) {
    if (product.quantityCart - 1 > 0) {
      product.quantityCart -= 1;
    }
  }
  plus(product: any) {
    if (product.quantityCart < product.quantity) {
      product.quantityCart += 1;
    }
  }
  addToCart(product: any) {
    if(product.accountId===this.account.id)
    {
      this.mesage.warn('Bạn không thể thêm sản phẩm của bạn')
    }
    else
    {
      this.cartService
      .addToCartWithQuantity(this.account.id, product.id, product.quantityCart)
      .subscribe((res: any) => {
        this.mesage.addToCart();
      });
    }
  }

  saveComment() {
    this.comment.accountId = this.account.id;
    this.comment.productId = this.id;
    this.commentService.save(this.comment).subscribe((res: any) => {
      this.mesage.success('Cảm ơn ý kiến đóng góp của bạn!');
    });
  }

  getSameProducts(product: any) {
    console.log(this.product);
    this.productService
      .findAll(
        '',
        '',
        product.categoryId,
        '',
        '',
        '',
        0,
        5,
        'createdDate',
        'desc'
      )
      .subscribe((res: any) => {
        for (var item of res.content) {
          if (item.id !== product.id) {
            this.sameProducts.push(item);
          }
        }
      });
  }

  addToCart1(item: any) {
    if(item.accountId===this.account.id)
    {
      this.mesage.warn('Bạn không thể thêm sản phẩm của bạn')
    }
    else
    {
      this.cartService.addToCart(this.account.id, item.id).subscribe((res: any) => {
        this.mesage.addToCart();
      });
    }
  }

  changePath(id: any) {
    this.sameProducts = [];
    this.getDetail(id);
    this.account = this.accountService.getCurrentUser();
    if (this.account) {
      this.commentService
        .allow(this.account.id, this.id)
        .subscribe((res: any) => {
          this.allow = res;
        });
      this.commentService
        .findAll(this.id, '', 0, this.size)
        .subscribe((res: any) => {
          this.comments = res.content;
        });
    }
  }

  payment(product: any) {
    if(product.accountId===this.account.id)
    {
      this.mesage.warn('Bạn không thể mua sản phẩm của bạn')
    }
    else
    {
      if (!this.otherAddress) {
        this.order.phone = this.account.userDTO.phoneNumber;
        this.order.address = this.account.userDTO.address;
        this.order.fullName = this.account.userDTO.fullName;
      }
      this.order.customerId = this.account.id;
      var list = [];
      list.push(product);
      this.order.products = list;
      this.cartService.buy(this.order).subscribe((res: any) => {
        this.mesage.paymentSuccess();
        setTimeout(() => {
          window.location.reload();
        }, 700);
      });
    }
  }
  changeAddress() {
    this.otherAddress = !this.otherAddress;
  }
}
