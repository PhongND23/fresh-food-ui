import { Component, OnInit } from '@angular/core';
import { BlogService } from '../service/blogservice';
import { Router } from '@angular/router';
@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css'],
})
export class BlogComponent implements OnInit {
  constructor(private blogService: BlogService, private router: Router) {}

  posts: any = [];
  categoryId: any = '';
  title: any = '';
  totalPages: any;
  totalElements: any;
  categories: any = [];
  page: any = 0;
  hotPosts:any=[]
  ngOnInit(): void {
    this.findAll();
    this.getHotPosts();
    this.blogService.drop().subscribe((res: any) => {
      this.categories = res;
    });
  }

  findAll() {
    this.blogService
      .findAll(this.title, this.categoryId, '',false, this.page, 6)
      .subscribe((res: any) => {
        this.posts = res.content;
        this.totalElements = res.totalElements;
        this.totalPages = res.totalPages;
      });
  }

  getDetail(id: any) {
    this.router.navigate(['/blog-detail', id]);
  }
  getByCategory(id: any) {
    this.categoryId = id;
    this.findAll();
  }
  getAll() {
    this.categoryId = '';
    this.findAll();
  }
  search() {
    this.findAll();
  }
  getPage(page: any) {
    this.page = page;
    this.findAll();
  }

  counter(number: any) {
    return new Array(number);
  }
  getHotPosts() {
    this.blogService
      .findAll(this.title, '', '',true, 0, 5)
      .subscribe((res: any) => {
        this.hotPosts = res.content;
      });
  }
}
